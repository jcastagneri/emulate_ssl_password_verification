"""
    server.py - host an SSL server that checks passwords

    CSCI 3403
    Authors: Matt Niemiec and Abigail Fernandes
    Number of lines of code in solution: 140
        (Feel free to use more or less, this
        is provided as a sanity check)

    Put your team members' names:
        Joe Castagneri
        Chen Kakam
        Qiu Duan
        Lauren Raddatz
"""

import hashlib
import os
import pickle
import socket

from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA

host = "localhost"
port = 10001


# A helper function. It may come in handy when performing symmetric encryption
def pad_message(message):
    return message + " " * ((16 - len(message)) % 16)


# Read server's private key from PROJECT_ROOT/server/server_key_rsa
def read_private_key():
    dir_name = os.path.dirname(os.path.abspath(__file__))
    private_key_path = os.path.join(dir_name, 'server_key_rsa')
    with open(private_key_path, 'rb') as f:
        private_key = RSA.importKey(f.read())
    return private_key


# DONE: Write a function that decrypts a message using the server's private key
def decrypt_key(session_key):
    private_key = read_private_key()
    cipher = PKCS1_OAEP.new(private_key)
    return cipher.decrypt(session_key)


# DONE: Write a function that decrypts a message using the session key
def decrypt_message(client_dict, session_key):
    cipher = AES.new(session_key, AES.MODE_EAX, nonce=client_dict['nonce'])
    plaintext = cipher.decrypt(client_dict['ciphertext'])

    # Verify the MAC signature
    try:
        cipher.verify(client_dict['MAC'])
    except ValueError:
        print("Key is incorrect or message was corrupted.")
        return False
    return plaintext.decode('utf8')


# DONE: Encrypt a message using the session key
def encrypt_message(message, session_key):
    cipher = AES.new(session_key, AES.MODE_EAX)
    nonce = cipher.nonce
    ciphertext, MAC = cipher.encrypt_and_digest(message)

    # Return nonce, cipher, and MAC within a serializable structure
    return {'nonce': nonce,
            'ciphertext': ciphertext,
            'MAC': MAC}


# Receive 1024 bytes from the client
def receive_message(connection):
    return connection.recv(1024)


# Sends message to client
def send_message(connection, data):
    if not data:
        print("Can't send empty string")
        return
    if type(data) != bytes:
        data = data.encode()
    connection.sendall(data)

# A function that reads in the password file, salts and hashes the password, and
# checks the stored hash of the password to see if they are equal. It returns
# True if they are and False if they aren't
def verify_hash(user, password):
    try:
        abs_path_base = os.path.dirname(os.path.abspath(__file__))
        passfile_path = os.path.join(abs_path_base, 'passfile.txt')
        reader = open(passfile_path, 'r')
        for line in reader.read().split('\n'):
            line = line.split("\t")
            if line[0] == user:
                salt = line[1]
                hashed_password = line[2]

                return hashed_password == hashlib.sha512((password + salt).
                                          encode()).hexdigest()
        reader.close()
    except FileNotFoundError:
        print('passfile.txt not found')
        return False
    return False


def main():
    # Set up network connection listener
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (host, port)
    print('starting up on {} port {}'.format(*server_address))
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(server_address)
    sock.listen(1)

    try:
        while True:
            # Wait for a connection
            print('waiting for a connection')
            connection, client_address = sock.accept()
            try:
                print('connection from', client_address)

                # Receive encrypted key from client
                encrypted_key = receive_message(connection)

                # Send okay back to client
                send_message(connection, "okay")

                # Decrypt key from client
                plaintext_key = decrypt_key(encrypted_key)

                # Receive encrypted message from client
                ciphertext_message = receive_message(connection)
                ciphertext_dict = pickle.loads(ciphertext_message)

                # Decrypt message from client
                plaintext_message = decrypt_message(ciphertext_dict,
                                                    plaintext_key)
                # If MAC fails to verify, close connection
                if not plaintext_message:
                    break

                # Split response from user into the username and password
                user, password = plaintext_message.split()
                if verify_hash(user, password):
                    plaintext_response = "User successfully authenticated!"
                else:
                    plaintext_response = "Password or username incorrect"

                # Encrypt response to client
                ciphertext_response = encrypt_message(plaintext_response.
                                                encode('utf8'), plaintext_key)

                # Send encrypted response
                send_message(connection, pickle.dumps(ciphertext_response))
            finally:
                # Clean up the connection
                connection.close()
    finally:
        sock.close()


if __name__ in "__main__":
    main()

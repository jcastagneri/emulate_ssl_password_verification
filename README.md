# Emulating SSL
This is a project for a cybersecurity course that emulates an SSL connection handshake between client and
server. 

# Project Structure
1. `client`: Contains script client uses to connect with server as well as the server's public key.
2. `server`: Contains script to add users to the hashed passwords file, the hashed passwords file, the 
server's private key, and the script defining the server's behavior.
3. `Writeup.pdf`: Contains the project requirements.
4. `Project_3_SSL_Connections_Report.pdf`: A writeup describing the steps we took in this project.
5. `Pipfile`/`Pipfile.lock`: Files defining dependecies for the project.

# Dependencies
Libraries are managed with pipenv. Run `pipenv install` in the root directory followed by `pipenv shell` to 
drop in to a virtual environment with the relevant packages.

If you've freshly installed `pipenv` using `pip` and run into errors when running `pipenv install`, run
`pip install --user --upgrade pipenv` to upgrade `pipenv` to the newest version.

# Usage
Enter into the virtual environment with `pipenv shell`. Run `python server/server.py &` followed by 
`python client/client.py`. The two valid users are `joe crackable` and `lauren lauren`.

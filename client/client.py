"""
    client.py - Connect to an SSL server

    CSCI 3403
    Authors: Matt Niemiec and Abigail Fernandes
    Number of lines of code in solution: 117
        (Feel free to use more or less, this
        is provided as a sanity check)

    Put your team members' names:
        Joe Castagneri
        Chen Kakam
        Qiu Duan
        Lauren Raddatz
"""

import os
import pickle
import socket

from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes

host = "localhost"
port = 10001


# A helper function that you may find useful for AES encryption
def pad_message(message):
    return message + " "*((16-len(message))%16)


# DONE: Generate a random 128 bit AES key
def generate_key():
    # Generate random 128 bit secret key for AES encryption
    return get_random_bytes(16)


# Read server's public key from PROJECT_ROOT/client/server_key_rsa.pub
def read_public_key():
    dir_name = os.path.dirname(os.path.abspath(__file__))
    public_key_path = os.path.join(dir_name, 'server_key_rsa.pub')
    with open(public_key_path, 'rb') as f:
        public_key = RSA.importKey(f.read())
    return public_key


# DONE: Takes an AES session key and encrypts it using the server's
# DONE: public key and returns the value
def encrypt_handshake(session_key):
    public_key = read_public_key()
    # Instantiate a cipher object that encrypts for us
    cipher = PKCS1_OAEP.new(public_key)
    return cipher.encrypt(session_key)


# DONE: Encrypts the message using AES. Same as server function
def encrypt_message(message, session_key):
    cipher = AES.new(session_key, AES.MODE_EAX)
    nonce = cipher.nonce
    ciphertext, MAC = cipher.encrypt_and_digest(message)
    # Return nonce, cipher, and MAC within a serializable structure
    return {'nonce': nonce,
            'ciphertext': ciphertext,
            'MAC': MAC}


# DONE: Decrypts the message using AES. Same as server function
def decrypt_message(message, session_key):
    cipher = AES.new(session_key, AES.MODE_EAX, nonce=message['nonce'])
    plaintext = cipher.decrypt(message['ciphertext'])

    # Verify the MAC signature
    try:
        cipher.verify(message['MAC'])
    except ValueError:
        print("Key is incorrect or message was corrupted.")
        return False
    return plaintext.decode('utf8')


# Sends a message over TCP
def send_message(sock, message):
    sock.sendall(message)


# Receive a message from TCP
def receive_message(sock):
    data = sock.recv(1024)
    return data


def main():
    user = input("What's your username? ")
    password = input("What's your password? ")

    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Connect the socket to the port where the server is listening
    server_address = (host, port)
    print('connecting to {} port {}'.format(*server_address))
    sock.connect(server_address)

    try:
        # Message that we need to send
        message = user + ' ' + password
        # Pycrypto prefers utf8 encoded payloads
        message = message.encode('utf8')

        # DONE: Generate random AES key
        session_key = generate_key()

        # DONE: Encrypt the session key using server's public key
        # Read in the public key
        ciphertext_session_key = encrypt_handshake(session_key)

        # DONE: Initiate handshake
        send_message(sock, ciphertext_session_key)

        # Listen for okay from server (why is this necessary?)
        if receive_message(sock).decode() != "okay":
            print("Couldn't connect to server")
            exit(0)

        # DONE: Encrypt message and send to server
        ciphertext_dict = encrypt_message(message, session_key)
        send_message(sock, pickle.dumps(ciphertext_dict))

        # DONE: Receive and decrypt response from server and print
        ciphertext_message = receive_message(sock)
        ciphertext_dict = pickle.loads(ciphertext_message)

        plaintext_message = decrypt_message(ciphertext_dict,
                                            session_key)

        # DONE: print the plaintext message
        print(plaintext_message)

    finally:
        print('closing socket')
        sock.close()


if __name__ in "__main__":
    main()
